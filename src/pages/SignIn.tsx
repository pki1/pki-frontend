import React from "react";
import { Formik, Form } from "formik";
import { MyTextField } from "../components/MyTextField";
import { MyCheckbox } from "../components/MyCheckbox";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import * as yup from "yup";
import { RouteComponentProps } from "react-router-dom";
import { useLoginMutation } from "../generated/graphql";
import { setAccessToken } from "../utils/accessToken";
const validationSchema = yup.object({
  username: yup
    .string()
    .required("Username is required")
    .min(3)
    .max(25),
  password: yup
    .string()
    .required("Password is required")
    .min(3)
    .max(100)
});

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

const SignIn: React.FC<RouteComponentProps> = ({ history }) => {
  const [login] = useLoginMutation();
  const classes = useStyles({});
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Formik
          validateOnChange={true}
          initialValues={{
            username: "",
            password: "",
            remember: false
          }}
          validationSchema={validationSchema}
          onSubmit={async (data, { setSubmitting }) => {
            setSubmitting(true);
            try {
              const response = await login({
                variables: {
                  username: data.username,
                  password: data.password,
                  rememberMe: data.remember
                }
              });
              if (response && response.data) {
                setAccessToken(response.data.login.accessToken);
                history.push("/");
              }
            } catch (err) {
              console.log(err);
            }
            setSubmitting(false);
          }}
        >
          {({ isSubmitting }) => (
            <Form>
              <MyTextField placeholder="Username" name="username" />
              <MyTextField
                placeholder="Password"
                name="password"
                type="password"
              />
              <FormControlLabel
                control={<MyCheckbox name="remember" />}
                label="Remember me"
              />
              <Button
                disabled={isSubmitting}
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Sign In
              </Button>
            </Form>
          )}
        </Formik>
        <Grid container>
          <Grid item>
            <Link
              component="button"
              variant="body2"
              onClick={() => {
                history.push("/signup");
              }}
            >
              {"Don't have an account? Sign Up"}
            </Link>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};
export { SignIn };
