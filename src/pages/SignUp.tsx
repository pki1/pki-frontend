import React from "react";
import { Formik, Form } from "formik";
import { MyTextField } from "../components/MyTextField";
import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import * as yup from "yup";
import { RouteComponentProps } from "react-router-dom";
import { useRegisterMutation } from "../generated/graphql";
const validationSchema = yup.object({
  username: yup
    .string()
    .required("Username is required")
    .min(3)
    .max(25),
  password: yup
    .string()
    .required("Password is required")
    .min(3)
    .max(100)
});

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

const SignUp: React.FC<RouteComponentProps> = ({ history }) => {
  const [register] = useRegisterMutation();
  const classes = useStyles({});
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <Formik
          validateOnChange={true}
          initialValues={{
            username: "",
            password: "",
          }}
          validationSchema={validationSchema}
          onSubmit={async (data, { setSubmitting }) => {
            setSubmitting(true);
            try {
              const response = await register({
                variables: {
                  username: data.username,
                  password: data.password,
                }
              });
              if (response && response.data) {
                history.push("/signin");
              }
            } catch (err) {
              console.log(err);
            }
            setSubmitting(false);
          }}
        >
          {({ isSubmitting }) => (
            <Form>
              <MyTextField placeholder="Username" name="username" />
              <MyTextField
                placeholder="Password"
                name="password"
                type="password"
              />

              <Button
                disabled={isSubmitting}
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Sign up
              </Button>
            </Form>
          )}
        </Formik>
        <Grid container>
          <Grid item>
            <Link
              component="button"
              variant="body2"
              onClick={() => {
                history.push("/signin");
              }}
            >
              {"Already have an account? Sign in!"}
            </Link>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};
export { SignUp };
