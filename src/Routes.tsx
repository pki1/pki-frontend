import React from "react";
import { Redirect, BrowserRouter, Switch, Route } from "react-router-dom";
import { isAuthenticated } from "./utils/auth";
import { SignIn } from "./pages/SignIn";
import { SignUp } from "./pages/SignUp";
import { Homepage } from "./components/Homepage";
interface Props { }
const PrivateRoute: React.FC<any> = ({
  Component,
  ...rest
}: {
  Component: any;
  rest: any;
}) => (
    <Route
      {...rest}
      render={props => {
        if (isAuthenticated()) return <Component {...props} />;
        return (
          <Redirect
            to={{ pathname: "/signin", state: { from: props.location } }}
          />
        );
      }}
    />
  );

export const Routes: React.FC<Props> = () => {
  return (
    <BrowserRouter>
      <div>
        <Switch>
          <PrivateRoute path="/" exact Component={Homepage} />
          <Route path="/signin" component={SignIn} />
          <Route path="/signup" component={SignUp} />
          <Route render={() => <h3>No Match</h3>} />
        </Switch>
      </div>
    </BrowserRouter>
  );
};
