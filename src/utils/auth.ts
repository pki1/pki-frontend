//import Cookies from "js-cookie";
import { getAccessToken } from "./accessToken";
/*const getToken = () => Cookies.get("token");
const testSetToken = () => Cookies.set("token", "ola");
const testClearToken = () => Cookies.remove("token");*/
const isAuthenticated = () => !!getAccessToken();

export { isAuthenticated };
