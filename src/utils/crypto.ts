import bigInt from "big-integer"
const Crypto = window.crypto.subtle

const getKeys = async () => {                         // receiver keypair initialization
    var ecdh = await Crypto.generateKey({ name: 'ECDH', namedCurve: 'P-256' }, true, ['deriveBits'])
    var privkey = await Crypto.exportKey('pkcs8', ecdh.privateKey) // private key to be stored locally
    var pubkey = await Crypto.exportKey('raw', ecdh.publicKey)
    pubkey = compress(pubkey)                   // compress for to 33 bytes
    return { privkey: base64(privkey), pubkey: base64(pubkey) }
}

//pubkey = await expand(fromBase64(pubkey))   // expand compressed receiver public key


// Helpers
//@ts-ignore
function base64(buf: any) { return btoa(String.fromCharCode.apply(null, new Uint8Array(buf))) } // ArrayBuffer to base64 string

//@ts-ignore
function fromBase64(base64: any) {                   // base64 string to Uint8Array; for ArrayBuffer use fromBase64().buffer
    base64 = base64.replace(/_/g, '/').replace(/-/g, '+') // also accept base64url
    var bin = atob(base64), len = bin.length, r = new Uint8Array(len)
    for (var i = 0; i < len; i++) r[i] = bin.charCodeAt(i)
    return r
}
function hex(buf: any) { return Array.prototype.map.call(new Uint8Array(buf), x => ('0' + x.toString(16)).slice(-2)).join('') } // ArrayBuffer to hex string
function fromHex(hex: any) { return new Uint8Array(hex.match(/.{1,2}/g).map((x: any) => parseInt(x, 16))) }
function assert(condition: any, message: any) { if (!condition) throw message }

// ECDH public key compression
const compress = (pubkey: any) => {                     // compress ECDH public key
    var r = new Uint8Array(pubkey.slice(0, 33)) // use pubkey.x only
    assert(pubkey.byteLength === 65 && r[0] === 4, 'invalid EC public key')
    var signY = new Uint8Array(pubkey.slice(64))[0] & 0x01 // extract pubkey.y sign in first bit
    r[0] = 2 + signY                            // encode in byte[0]
    return r
}

//@ts-ignore
const expand = async (pubkey: any) => {                 // expand Uint8Array ECDH public key (based on https://stackoverflow.com/questions/17171542/algorithm-for-elliptic-curve-point-compression/30431547#30431547)
    assert(pubkey.length === 33 && (pubkey[0] === 2 || pubkey[0] === 3), 'invalid EC compressed key')
    //    if (!bigInt) bigInt = await import('./BigInteger.min.js') // import on demand 
    var two = bigInt(2)                     // http://peterolson.github.com/BigInteger.js/BigInteger.min.js
    let P256 = {                                // constants for P-256 curve
        prime: two.pow(256).subtract(two.pow(224)).add(two.pow(192)).add(two.pow(96)).subtract(1),
        b: bigInt('41058363725152142129326129780047268409114441015993725554835256314039467401291')
    } as any
    P256.pIdent = P256.prime.add(1).divide(4)
    var yminus = pubkey[0] === 3 ? true : false  // byte[0]: 2 or 3 (4 indicates an uncompressed key, and anything else is invalid)
    var xbig = bigInt(hex(pubkey.slice(1)), 16) // import x
    var ybig = xbig.pow(3).subtract(xbig.multiply(3)).add(P256.b).modPow(P256.pIdent, P256.prime) // y^2 = x^3 - 3x + b
    if (ybig.isOdd() !== yminus) ybig = P256.prime.subtract(ybig) // invert if parity doesn't match - it's the other root
    var y = ybig.toString(16)                   // hex string
    if (y.length < 64) y = new Array(64 - y.length + 1).join('0') + y // pad with '0'
    var r = new Uint8Array(65)                  // expanded key is 65 bytes
    r.set(pubkey)                               // byte[1-32]: x
    r[0] = 4                                    // byte[0]: indicator
    r.set(fromHex(y), 33)                       // byte[33-64]: y
    return r.buffer                             // return ArrayBuffer
}


export { getKeys }