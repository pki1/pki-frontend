import React, { useState } from "react";
import { Formik, Form } from "formik";
import { MyTextField } from "../components/MyTextField";
import Container from "@material-ui/core/Container";
import * as yup from "yup";
import CssBaseline from "@material-ui/core/CssBaseline";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {
  useMyCertificatesQuery,
  useLogoutMutation,
  useCreateCertMutation,
  useSearchCertificatesMutation
} from "../generated/graphql";
import { setAccessToken } from "../utils/accessToken";
import { makeStyles } from "@material-ui/core/styles";

import { RouteComponentProps } from "react-router-dom";
import { CustomTable } from "./CustomTable";
import { getKeys } from "src/utils/crypto";
import { Grid } from "@material-ui/core";
import { DevInfo } from "./DevInfo";

//@ts-ignore
const getDate = () => {
  const date = new Date();
  return (
    date.getFullYear() +
    "-" +
    (date.getMonth() + 1) +
    "-" +
    date.getDate() +
    "T" +
    date.getHours() +
    ":" +
    date.getMinutes() +
    ":" +
    date.getSeconds()
  );
};
const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  logoutBtn: {
    position: "absolute",
    right: 20,
    top: 20
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  loadingIndicator: {
    position: "absolute",
    top: "50%",
    left: "50%"
  },
  forms: {
    marginTop: 20,
    width: 1000
  },
  internalForm: {}
}));
const validationCreateCert = yup.object({
  validity: yup
    .date()
    .required("Date is required")
    .min(Date()),
  usage: yup
    .string()
    .required("Usage is required")
    .min(3)
    .max(200)
});
const validationSearchCert = yup.object({
  username: yup
    .string()
    .required("Username is required")
    .min(3)
    .max(25)
});

interface iCertificates {
  username: string;
  array: any;
}
const downloadTxtFile = (title: string, bodyText: string) => {
  const element = document.createElement("a");
  const file = new Blob([bodyText], { type: "text/plain" });
  element.href = URL.createObjectURL(file);
  element.download = `${title}.txt`;
  document.body.appendChild(element); // Required for this to work in FireFox
  element.click();
};
const Homepage: React.FC<RouteComponentProps> = ({ history }) => {
  const classes = useStyles();

  const { data, loading } = useMyCertificatesQuery({
    fetchPolicy: "network-only"
  });
  const [logout] = useLogoutMutation();
  const [createCert] = useCreateCertMutation();
  const [searchCertificates] = useSearchCertificatesMutation();
  const [certificates, setCertificates] = useState<iCertificates>({
    username: "",
    array: []
  });
  const [loadingIndicator, setLoadingIndicator] = useState(false);

  if (loading) return <CircularProgress className={classes.loadingIndicator} />;
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h1">
          Homepage
        </Typography>
        {loadingIndicator && (
          <CircularProgress className={classes.loadingIndicator} />
        )}
        <Grid container spacing={3} className={classes.forms}>
          <Grid className={classes.internalForm} item xs={8}>
            <Typography variant="h2">
              Search other people certificates
            </Typography>
            <Formik
              validateOnChange={true}
              initialValues={{
                username: ""
              }}
              validationSchema={validationSearchCert}
              onSubmit={async (data, { setSubmitting }) => {
                setSubmitting(true);
                setLoadingIndicator(true);
                try {
                  const response = await searchCertificates({
                    variables: { username: data.username }
                  });
                  if (
                    response &&
                    response.data &&
                    response.data.searchCertificates
                  ) {
                    const username = data.username;
                    const obj = {
                      username: username,
                      array: response.data.searchCertificates
                    };
                    setCertificates(obj);
                  }
                } catch (err) {
                  console.log(err);
                }

                setLoadingIndicator(false);
                setSubmitting(false);
              }}
            >
              {({ isSubmitting }) => (
                <Form>
                  <MyTextField
                    placeholder="Username To Search"
                    name="username"
                  />
                  <Button
                    disabled={isSubmitting}
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                  >
                    Search
                  </Button>
                </Form>
              )}
            </Formik>
            {certificates.array.length > 0 ? (
              <CustomTable
                title={`${certificates.username} Certificates`}
                data={certificates.array}
              />
            ) : null}
          </Grid>
          <Grid className={classes.internalForm} item xs={4}>
            <Typography variant="h2">Create Certificates</Typography>
            <Formik
              validateOnChange={true}
              initialValues={{
                validity: Date(),
                usage: ""
              }}
              validationSchema={validationCreateCert}
              onSubmit={async (data, { setSubmitting }) => {
                setSubmitting(true);
                const validity = new Date(data.validity).getTime();

                try {
                  const keys = await getKeys();
                  console.log(keys);
                  const response = await createCert({
                    variables: {
                      algorithm: "ECDH",
                      usage: data.usage,
                      pubKey: keys.pubkey,
                      validity
                    }
                  });
                  const obj = {
                    algorithm: "ECDH",
                    usage: data.usage,
                    pubKey: keys.pubkey,
                    privKey: keys.privkey,
                    validity
                  };
                  downloadTxtFile(
                    `${data.usage}_${getDate()}`,
                    JSON.stringify(obj)
                  );
                  console.log(response.data);
                  if (response && response.data && response.data.createCert) {
                    window.location.reload();
                  }
                } catch (err) {
                  console.log(err);
                }
                setSubmitting(false);
              }}
            >
              {({ isSubmitting }) => (
                <Form>
                  <Typography variant="body1">
                    Using algorithm ECDH to generate keys.
                  </Typography>
                  <MyTextField placeholder="Usage" name="usage" />
                  <MyTextField
                    name="validity"
                    id="datetime-local"
                    type="datetime-local"
                    //defaultValue={getDate()}
                    //defaultValue="2017-05-24T10:30"
                  />
                  <Button
                    disabled={isSubmitting}
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                  >
                    Create
                  </Button>
                </Form>
              )}
            </Formik>
          </Grid>
        </Grid>
        <CustomTable
          title={"My Certificates"}
          data={data!.myCertificates}
          editable={true}
        />
        <Button
          className={classes.logoutBtn}
          variant="contained"
          color="primary"
          onClick={async () => {
            try {
              const response = await logout();
              if (response && response.data) {
                setAccessToken("");
                history.push("/signin");
              }
            } catch (err) {
              console.log(err);
            }
          }}
        >
          Logout
        </Button>
      </div>
      <DevInfo />
    </Container>
  );
};

export { Homepage };
