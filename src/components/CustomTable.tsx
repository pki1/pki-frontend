import React from 'react';
import MaterialTable from 'material-table';

const CustomTable = (props: any) => {
    const [state, setState] = React.useState({
        columns: [
            { title: 'Version', field: 'version' },
            { title: 'User', field: 'user_identity' },
            { title: 'Algorithm', field: 'algorithm' },
            { title: 'Public Key', field: 'pub_key' },
            { title: 'Usage', field: 'usage' },
            { title: 'Validity', field: 'validity' }
        ],
        data: props.data,
    });

    return (
        <MaterialTable
            title={props.title}
            //@ts-ignore
            columns={state.columns}
            data={state.data}
            editable={props.editable ? {
                onRowAdd: newData =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            setState(prevState => {
                                const data = [...prevState.data];
                                data.push(newData);
                                return { ...prevState, data };
                            });
                        }, 600);
                    })/*,
                onRowUpdate: (newData, oldData) =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            if (oldData) {
                                setState(prevState => {
                                    const data = [...prevState.data];
                                    data[data.indexOf(oldData)] = newData;
                                    return { ...prevState, data };
                                });
                            }
                        }, 600);
                    }),
                onRowDelete: oldData =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            setState(prevState => {
                                const data = [...prevState.data];
                                data.splice(data.indexOf(oldData), 1);
                                return { ...prevState, data };
                            });
                        }, 600);
                    }),*/
            } : {}}
        />
    )
}
export { CustomTable }