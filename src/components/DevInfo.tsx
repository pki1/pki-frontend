import React from "react";
import Typography from "@material-ui/core/Typography";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { makeStyles } from "@material-ui/core/styles";
import GitHubIcon from "@material-ui/icons/GitHub";
import Avatar from "@material-ui/core/Avatar";
import Tooltip from "@material-ui/core/Tooltip";

const useStyles = makeStyles(theme => ({
    footer: {
        padding: 1,
        height: '4vh',
        position: "absolute",
        alignItems: "center",
        left: 0,
        bottom: 0,
        right: 0,
        color: "white"
    },
    avatar: {
        margin: theme.spacing(1),
        marginLeft: 30,
        marginRight: 30,
        backgroundColor: "#95a5a6"
    }
}));

const DevInfo = () => {
    const classes = useStyles({});

    return (
        <AppBar position="static" className={classes.footer}>
            <Toolbar variant="dense">
                <Typography variant="body2" align="center">
                    José Donato
                </Typography>

                <a
                    rel="noopener noreferrer"
                    href="https://github.com/jose-donato/"
                    target="_blank"
                >
                    <Tooltip title="Click to check my Github">
                        <Avatar className={classes.avatar}>
                            <GitHubIcon />
                        </Avatar>
                    </Tooltip>
                </a>
                <Typography variant="body2" align="center">
                    {new Date().getFullYear()}
                </Typography>
            </Toolbar>
        </AppBar>
    );
};

export { DevInfo };
