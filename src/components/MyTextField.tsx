import React from "react";
import { FieldAttributes, useField } from "formik";
import TextField from "@material-ui/core/TextField";
export const MyTextField: React.FC<FieldAttributes<{}>> = ({
  placeholder,
  type,
  ...otherProps
}) => {
  const [field, meta] = useField<{}>(otherProps);
  const errorText = meta.error && meta.touched ? meta.error : "";
  return (
    <TextField
      placeholder={placeholder}
      type={type}
      {...field}
      helperText={errorText}
      error={!!errorText}
      variant="outlined"
      margin="normal"
      required
      fullWidth
      autoFocus
    />
  );
};
