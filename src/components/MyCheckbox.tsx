import React from "react";
import MuiCheckbox from "@material-ui/core/Checkbox";
import { useField } from "formik";

const MyCheckbox = ({ ...props }) => {
  const [field] = useField(props.name);

  return <MuiCheckbox color="primary" {...field} checked={field.value} />;
};

export { MyCheckbox };
