import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
};

export type CertificateS = {
   __typename?: 'CertificateS',
  version: Scalars['String'],
  user_identity: Scalars['String'],
  algorithm: Scalars['String'],
  validity: Scalars['Float'],
  pub_key: Scalars['String'],
  issuer_id: Scalars['String'],
  usage: Scalars['String'],
  hashing_algorithm: Scalars['String'],
};

export type LoginResponse = {
   __typename?: 'LoginResponse',
  accessToken: Scalars['String'],
};

export type Mutation = {
   __typename?: 'Mutation',
  searchCertificates: Array<CertificateS>,
  register: Scalars['Boolean'],
  login: LoginResponse,
  logout: Scalars['Boolean'],
  createCert: Scalars['Boolean'],
};


export type MutationSearchCertificatesArgs = {
  username: Scalars['String']
};


export type MutationRegisterArgs = {
  password: Scalars['String'],
  username: Scalars['String']
};


export type MutationLoginArgs = {
  rememberMe: Scalars['Boolean'],
  password: Scalars['String'],
  username: Scalars['String']
};


export type MutationCreateCertArgs = {
  usage: Scalars['String'],
  pubKey: Scalars['String'],
  validity: Scalars['Float'],
  algorithm: Scalars['String']
};

export type Query = {
   __typename?: 'Query',
  hello: Scalars['String'],
  users: Array<User>,
  me: Scalars['String'],
  myCertificates: Array<CertificateS>,
  allCertificates: Array<CertificateS>,
};

export type User = {
   __typename?: 'User',
  id: Scalars['Int'],
  name: Scalars['String'],
  username: Scalars['String'],
};

export type CreateCertMutationVariables = {
  algorithm: Scalars['String'],
  validity: Scalars['Float'],
  pubKey: Scalars['String'],
  usage: Scalars['String']
};


export type CreateCertMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'createCert'>
);

export type LoginMutationVariables = {
  username: Scalars['String'],
  password: Scalars['String'],
  rememberMe: Scalars['Boolean']
};


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & { login: (
    { __typename?: 'LoginResponse' }
    & Pick<LoginResponse, 'accessToken'>
  ) }
);

export type LogoutMutationVariables = {};


export type LogoutMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'logout'>
);

export type MyCertificatesQueryVariables = {};


export type MyCertificatesQuery = (
  { __typename?: 'Query' }
  & { myCertificates: Array<(
    { __typename?: 'CertificateS' }
    & Pick<CertificateS, 'version' | 'pub_key' | 'user_identity' | 'algorithm' | 'validity' | 'issuer_id' | 'usage'>
  )> }
);

export type RegisterMutationVariables = {
  username: Scalars['String'],
  password: Scalars['String']
};


export type RegisterMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'register'>
);

export type SearchCertificatesMutationVariables = {
  username: Scalars['String']
};


export type SearchCertificatesMutation = (
  { __typename?: 'Mutation' }
  & { searchCertificates: Array<(
    { __typename?: 'CertificateS' }
    & Pick<CertificateS, 'version' | 'pub_key' | 'user_identity' | 'algorithm' | 'validity' | 'issuer_id' | 'usage'>
  )> }
);


export const CreateCertDocument = gql`
    mutation createCert($algorithm: String!, $validity: Float!, $pubKey: String!, $usage: String!) {
  createCert(algorithm: $algorithm, validity: $validity, pubKey: $pubKey, usage: $usage)
}
    `;
export type CreateCertMutationFn = ApolloReactCommon.MutationFunction<CreateCertMutation, CreateCertMutationVariables>;

/**
 * __useCreateCertMutation__
 *
 * To run a mutation, you first call `useCreateCertMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCertMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCertMutation, { data, loading, error }] = useCreateCertMutation({
 *   variables: {
 *      algorithm: // value for 'algorithm'
 *      validity: // value for 'validity'
 *      pubKey: // value for 'pubKey'
 *      usage: // value for 'usage'
 *   },
 * });
 */
export function useCreateCertMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateCertMutation, CreateCertMutationVariables>) {
        return ApolloReactHooks.useMutation<CreateCertMutation, CreateCertMutationVariables>(CreateCertDocument, baseOptions);
      }
export type CreateCertMutationHookResult = ReturnType<typeof useCreateCertMutation>;
export type CreateCertMutationResult = ApolloReactCommon.MutationResult<CreateCertMutation>;
export type CreateCertMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateCertMutation, CreateCertMutationVariables>;
export const LoginDocument = gql`
    mutation Login($username: String!, $password: String!, $rememberMe: Boolean!) {
  login(username: $username, password: $password, rememberMe: $rememberMe) {
    accessToken
  }
}
    `;
export type LoginMutationFn = ApolloReactCommon.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *      rememberMe: // value for 'rememberMe'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        return ApolloReactHooks.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, baseOptions);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = ApolloReactCommon.MutationResult<LoginMutation>;
export type LoginMutationOptions = ApolloReactCommon.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const LogoutDocument = gql`
    mutation Logout {
  logout
}
    `;
export type LogoutMutationFn = ApolloReactCommon.MutationFunction<LogoutMutation, LogoutMutationVariables>;

/**
 * __useLogoutMutation__
 *
 * To run a mutation, you first call `useLogoutMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLogoutMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [logoutMutation, { data, loading, error }] = useLogoutMutation({
 *   variables: {
 *   },
 * });
 */
export function useLogoutMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LogoutMutation, LogoutMutationVariables>) {
        return ApolloReactHooks.useMutation<LogoutMutation, LogoutMutationVariables>(LogoutDocument, baseOptions);
      }
export type LogoutMutationHookResult = ReturnType<typeof useLogoutMutation>;
export type LogoutMutationResult = ApolloReactCommon.MutationResult<LogoutMutation>;
export type LogoutMutationOptions = ApolloReactCommon.BaseMutationOptions<LogoutMutation, LogoutMutationVariables>;
export const MyCertificatesDocument = gql`
    query myCertificates {
  myCertificates {
    version
    pub_key
    user_identity
    algorithm
    validity
    issuer_id
    usage
  }
}
    `;

/**
 * __useMyCertificatesQuery__
 *
 * To run a query within a React component, call `useMyCertificatesQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyCertificatesQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyCertificatesQuery({
 *   variables: {
 *   },
 * });
 */
export function useMyCertificatesQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<MyCertificatesQuery, MyCertificatesQueryVariables>) {
        return ApolloReactHooks.useQuery<MyCertificatesQuery, MyCertificatesQueryVariables>(MyCertificatesDocument, baseOptions);
      }
export function useMyCertificatesLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<MyCertificatesQuery, MyCertificatesQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<MyCertificatesQuery, MyCertificatesQueryVariables>(MyCertificatesDocument, baseOptions);
        }
export type MyCertificatesQueryHookResult = ReturnType<typeof useMyCertificatesQuery>;
export type MyCertificatesLazyQueryHookResult = ReturnType<typeof useMyCertificatesLazyQuery>;
export type MyCertificatesQueryResult = ApolloReactCommon.QueryResult<MyCertificatesQuery, MyCertificatesQueryVariables>;
export const RegisterDocument = gql`
    mutation Register($username: String!, $password: String!) {
  register(username: $username, password: $password)
}
    `;
export type RegisterMutationFn = ApolloReactCommon.MutationFunction<RegisterMutation, RegisterMutationVariables>;

/**
 * __useRegisterMutation__
 *
 * To run a mutation, you first call `useRegisterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerMutation, { data, loading, error }] = useRegisterMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useRegisterMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<RegisterMutation, RegisterMutationVariables>) {
        return ApolloReactHooks.useMutation<RegisterMutation, RegisterMutationVariables>(RegisterDocument, baseOptions);
      }
export type RegisterMutationHookResult = ReturnType<typeof useRegisterMutation>;
export type RegisterMutationResult = ApolloReactCommon.MutationResult<RegisterMutation>;
export type RegisterMutationOptions = ApolloReactCommon.BaseMutationOptions<RegisterMutation, RegisterMutationVariables>;
export const SearchCertificatesDocument = gql`
    mutation searchCertificates($username: String!) {
  searchCertificates(username: $username) {
    version
    pub_key
    user_identity
    algorithm
    validity
    issuer_id
    usage
  }
}
    `;
export type SearchCertificatesMutationFn = ApolloReactCommon.MutationFunction<SearchCertificatesMutation, SearchCertificatesMutationVariables>;

/**
 * __useSearchCertificatesMutation__
 *
 * To run a mutation, you first call `useSearchCertificatesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSearchCertificatesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [searchCertificatesMutation, { data, loading, error }] = useSearchCertificatesMutation({
 *   variables: {
 *      username: // value for 'username'
 *   },
 * });
 */
export function useSearchCertificatesMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<SearchCertificatesMutation, SearchCertificatesMutationVariables>) {
        return ApolloReactHooks.useMutation<SearchCertificatesMutation, SearchCertificatesMutationVariables>(SearchCertificatesDocument, baseOptions);
      }
export type SearchCertificatesMutationHookResult = ReturnType<typeof useSearchCertificatesMutation>;
export type SearchCertificatesMutationResult = ApolloReactCommon.MutationResult<SearchCertificatesMutation>;
export type SearchCertificatesMutationOptions = ApolloReactCommon.BaseMutationOptions<SearchCertificatesMutation, SearchCertificatesMutationVariables>;