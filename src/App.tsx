import React, { useEffect, useState } from "react";
//import { CustomSnackbar } from "./components/custom-snackbar.component";
import "./App.css";
import { setAccessToken } from "./utils/accessToken";
import CircularProgress from "@material-ui/core/CircularProgress";
import axios from "axios";
import { Routes } from "./Routes";
export const App = () => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const a = await axios.post(
        "https://cry-pki-server.herokuapp.com/refresh_token",
        {},
        { withCredentials: true }
      );
      setAccessToken(a.data.accessToken);
      setLoading(false);
    };
    fetchData();
  }, []);
  /*
    fetch("http://localhost:5000/refresh_token", {
      method: "POST",
      credentials: "include"
    }).then(async x => {
      console.log(x);
      const { accessToken } = await x.json();
      setAccessToken(accessToken);
      setLoading(false);
    });
  }, []);*/
  if (loading) return <CircularProgress />;
  //<CustomSnackbar type={"none"} message={"none"} />
  return <Routes />;
};
